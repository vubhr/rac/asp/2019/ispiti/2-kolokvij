#include <iostream>
#include <string>
#include <vector>

struct Osoba {
  std::string ime;
  int visina;
};

// (3b) Za (sortirani) vektor iz prošlog zadatka izradite funkciju koja će pomoću binarnog
// pretraživanja pronaći prvu osobu s određenom visinom. Izbjegnite svako nepotrebno kopiranje
// podataka. Funkcija vraća Osobu ukoliko je pronađena, { "", 0 } ukoliko nije.
Osoba pronadjiOsobu(const std::vector<Osoba> &osobe, int visina, int pocetak, int kraj) {
  int sredina = (kraj - pocetak) / 2;
  if (osobe[sredina].visina == visina) {
    return osobe[sredina];
  }
  if (pocetak == kraj || pocetak + 1 == kraj) {
    return { "", 0 };
  }
  
  if (visina < osobe[sredina].visina) {
    return pronadjiOsobu(osobe, visina, pocetak, sredina);
  } else {
    return pronadjiOsobu(osobe, visina, sredina, kraj);
  }
}

int main() {
  std::vector<Osoba> osobe = {
    { "Luka", 172 },
    { "Marko", 182 },
    { "Ivan", 184 },
    { "Hrvoje", 191 }
  };

  // (1b) Prikažite korištenje funkcije.
  Osoba osoba = pronadjiOsobu(osobe, 182, 0, osobe.size() - 1);
  std::cout << osoba.ime << ":" << osoba.visina << std::endl;
}