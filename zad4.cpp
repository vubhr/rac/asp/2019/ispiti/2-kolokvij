#include <iostream>
#include <string>
#include <vector>
#include <map>

// (2b) Napišite funkciju koja će prebaciti postavke iz vectora u mapu. Funkciji kao argumente
// predajete i vector i mapu.
void prebaciPostavke(const std::vector<std::string> &postavke, std::map<std::string, std::string> &mapaPostavki) {
  for (int i = 0; i < postavke.size(); i += 2) {
    mapaPostavki[postavke[i]] = postavke[i+1];
  }
}

// (1b) Napišite funkciju koja iz mape postavaka dohvaća željenu postavku: std::string
// dohvatiPostavku(?, std::string postavka);. Podrazumijeva se da se tražena postavka
// nalazi u mapi.
std::string dohvatiPostavku(std::map<std::string, std::string> &mapaPostavki, std::string postavka) {
  return mapaPostavki[postavka];
}

int main() {
  // (1b) Definiran je vector<std::string> koji sadrži postavke neke aplikacije.
  // Jedan element je oznaka postavke, a sljedeći element je vrijednost, npr.: "DATABASE", "localhost",
  // "USERNAME", "boss", .... Definirajte vector.
  std::vector<std::string> postavke = {
    "DATABASE", "localhost",
    "USERNAME", "boss",
    "PASSWORD", "zaporka123"
  };

  // (1b) Definirajte map koji može pohraniti spomenute postavke na efikasniji način. Ispravno definirajte map.
  std::map<std::string, std::string> mapaPostavki;

  // (1b) Prikažite korištenje funkcija u glavnoj main funkciji.
  prebaciPostavke(postavke, mapaPostavki);

  std::cout << dohvatiPostavku(mapaPostavki, "PASSWORD") << std::endl;
}