#include <iostream>
#include <set>

// (2b) Izradite funkciju koja ubacuje elemente u set. 
// Funkcija vraća false ukoliko se element već nalazi u setu (provjeriti!). 
// Funkcija vraća false ukoliko više nema mjesta u setu (maksimum je 7 brojeva). Vraća true ukoliko je uspješno unesen element.
bool ubaciBroj(std::set<int> &brojevi, int broj) {
  if (brojevi.size() >= 7) {
    return false;
  }
  auto result = brojevi.insert(broj);
  return result.second;
}

// (1b) Izradite funkciju koja prikazuje elemente silaznim poretkom (predlaže se korištenje iteratora)
void prikaziBrojeve(const std::set<int> &brojevi) {
  for (auto it = brojevi.rbegin(); it != brojevi.rend(); it++) {
    std::cout << *it << std::endl;
  }
}

int main() {
  // (1b) Izradite set koji omogućava pohranu cijelih brojeva. set sadrži brojeve lota.
  std::set<int> brojeviLota;

  // (1b) Prikažite korištenje funkcija u glavnoj main funkciji.
  ubaciBroj(brojeviLota, 4);
  ubaciBroj(brojeviLota, 21);
  ubaciBroj(brojeviLota, 21);
  ubaciBroj(brojeviLota, 2);
  ubaciBroj(brojeviLota, 45);

  prikaziBrojeve(brojeviLota);
}