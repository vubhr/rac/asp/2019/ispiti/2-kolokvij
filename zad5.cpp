#include <iostream>
#include <string>
#include <vector>

// (1b) Zadana je struktura struct Osoba std::string ime, int visina ;. vector sadrži
// neodređen broj Osoba. Definirajte strukturu i vector koji sadrži nekoliko Osoba.
struct Osoba {
  std::string ime;
  int visina;
};

// (3b) Izradite funkciju koja će pomoću bubble sort algoritma sortirati osobe u vectoru uzlazno prema visini osobe.
// https://repl.it/@KrunoslavHusak/asp12bubble
void bubbleSort(std::vector<Osoba> &osobe) {
  Osoba tmp;
  while (true) {
    bool zamjena = false;

    for (int i = 0; i < osobe.size() - 1; i++) {
      if (osobe[i].visina > osobe[i+1].visina) {
        tmp = osobe[i];
        osobe[i] = osobe[i+1];
        osobe[i+1] = tmp;
        zamjena = true;
      }
    }

    if (!zamjena) {
      break;
    }
  }
}

int main() {
  std::vector<Osoba> osobe = {
    { "Luka", 172 },
    { "Hrvoje", 191 },
    { "Marko", 182 },
    { "Ivan", 184 },
  };

  bubbleSort(osobe);

  for (auto osoba : osobe) {
    std::cout << osoba.ime << ":" << osoba.visina << std::endl;
  }
}